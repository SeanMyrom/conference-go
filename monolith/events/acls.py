import json
import requests
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={OPEN_WEATHER_API_KEY}"
    # convert city/state to lat, lon
        #make HTTP request to geocode API
        # grab lat and lon from the API response
    # get the weather by lat and lon
        #make HTTP request to weather API
        # grab weather from the API response
    # return the weather
    r = requests.get(geo_url)
    body = r.json()

    try:
        latitude = body[0]["lat"]
        longitude = body[0]["lon"]
    except (KeyError, IndexError):
        return None

    parameters = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
        }

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url, params=parameters)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "query": city + " " + state,
        "per_page": 1
        }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
    # anti-corruption lines
#     picture_url = pexel_dict["photos"][0]["src"]["original"]
#     return {"picture_url": picture_url}

# # headers = {
# #     "Authorization": PEXELS_API_KEY
# # }
